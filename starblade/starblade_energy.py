# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2017-2018 Max-Planck-Society
# Author: Jakob Knollmueller
#
# Starblade is being developed at the Max-Planck-Institut fuer Astrophysik

from nifty4 import Energy, Field, log, exp, DiagonalOperator,\
    create_power_operator, SandwichOperator, ScalingOperator, InversionEnabler#, SamplingEnabler
from nifty4.library.nonlinearities import PositiveTanh, Tanh


class StarbladeEnergy(Energy):
    """The Energy for the starblade problem.

    It implements the Information Hamiltonian of the separation of d

    Parameters
    ----------
    position : Field
        The current position of the separation.
    parameters : Dictionary
        Dictionary containing all relevant quantities for the inference,
        data : Field
            The image data.
        alpha : Field
            Slope parameter of the point-source prior
        q : Field
            Cutoff parameter of the point-source prior
        power_spectrum : callable or Field
            An object that contains the power spectrum of the diffuse component
             as a function of the harmonic mode.
        FFT : FFTOperator
            An operator performing the Fourier transform
        inverter : ConjugateGradient
            The minimization strategy to use for operator inversion
        newton_iterations :
            Number of Newton optimization steps.
        sampling_inverter :
            Inverter which is used to generate samples.
    """

    def __init__(self, position, parameters):

        x = position.val.clip(-9, 9)
        position = Field(position.domain, val=x)
        super(StarbladeEnergy, self).__init__(position=position)

        self.parameters = parameters
        self.inverter = parameters['inverter']
        self.d = parameters['data']
        self.FFT = parameters['FFT']
        self.power_spectrum = parameters['power_spectrum']
        self.correlation = create_power_operator(self.FFT.domain, self.power_spectrum)
        self.alpha = parameters['alpha']
        self.q = parameters['q']
        self.update_power = parameters['update_power']
        self.newton_iterations = parameters['newton_iterations']
        pos_tanh = PositiveTanh()
        tanh = Tanh()
        self.S = SandwichOperator.make(self.FFT.adjoint, self.correlation)
        self.a = pos_tanh(self.position)
        self.a_p = pos_tanh.derivative(self.position)
        self.a_pp = -tanh(position)*tanh.derivative(self.position)
        self.u = log(self.d * self.a)
        self.u_p = self.a_p/self.a
        self.u_a = -log(self.a)
        self.u_ap = - self.a_p/self.a
        one_m_a = 1 - self.a
        self.s = log(self.d * one_m_a)
        self.s_p = - self.a_p / one_m_a
        self.var_x = 9.

    def at(self, position):
        return self.__class__(position, parameters=self.parameters)

    @property
    def value(self):
        diffuse = 0.5 * self.s.vdot(self.S.inverse(self.s))
        point = (self.alpha-1).vdot(self.u) + self.q.vdot(exp(-self.u))
        det = - self.s.sum()
        det += - self.u_a.sum()
        det += -log(self.a_p).sum()
        det += 0.5 / self.var_x * self.position.vdot(self.position)
        return diffuse + point + det

    @property
    def gradient(self):
        diffuse = self.S.inverse(self.s) * self.s_p
        point = (self.alpha - 1) * self.u_p - self.q * exp(-self.u) * self.u_p
        det = self.position / self.var_x
        det += - self.s_p
        det += - self.u_ap
        det += -1./self.a_p * self.a_pp

        return +diffuse + point + det

    @property
    def curvature(self):
        point = self.q * exp(-self.u) * self.u_p ** 2

        O_x = DiagonalOperator(Field(self.position.domain, val=1./self.var_x))
        N_inv = DiagonalOperator(point)
        R = ScalingOperator(1., point.domain)
        S_p = DiagonalOperator(self.s_p)
        my_S_inv = SandwichOperator.make(self.FFT.adjoint.inverse.adjoint*S_p, self.correlation.inverse)
        curv = InversionEnabler(my_S_inv+N_inv, self.inverter,O_x)
        return curv
