# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2017-2018 Max-Planck-Society
# Author: Jakob Knollmueller
#
# Starblade is being developed at the Max-Planck-Institut fuer Astrophysik

from nifty4 import Energy, Field,  DiagonalOperator, InversionEnabler, full, ScalingOperator
from .starblade_energy import StarbladeEnergy


class StarbladeKL(Energy):
    """The Kullback-Leibler divergence for the starblade problem.

    Parameters
    ----------
    position : Field
        The current position of the separation.
    samples : List
        A list containing residual samples.
    parameters : Dictionary
        Dictionary containing all relevant quantities for the inference,
        data : Field
            The image data.
        alpha : Field
            Slope parameter of the point-source prior
        q : Field
            Cutoff parameter of the point-source prior
        power_spectrum : callable or Field
            An object that contains the power spectrum of the diffuse component
             as a function of the harmonic mode.
        FFT : FFTOperator
            An operator performing the Fourier transform
        inverter : ConjugateGradient
            the minimization strategy to use for operator inversion
        newton_iterations :
            Number of Newton optimization steps.
    """

    def __init__(self, position, samples, parameters):
        super(StarbladeKL, self).__init__(position=position)
        self.samples = samples
        self.parameters = parameters
        self.energy_list = []
        for sample in samples:
            energy = StarbladeEnergy(position+sample,parameters)
            self.energy_list.append(energy)

        self.ScaleOp = ScalingOperator(1./len(self.energy_list),self.position.domain)

    def at(self, position):
        return self.__class__(position, samples=self.samples, parameters=self.parameters)

    @property
    def value(self):
        value = 0.
        for energy in self.energy_list:
            value += energy.value
        value /= len(self.energy_list)
        return value

    @property
    def gradient(self):
        gradient = full(self.position.domain, 0.)
        for energy in self.energy_list:
            gradient += energy.gradient
        return self.ScaleOp(gradient)

    @property
    def curvature(self):
        curvature = DiagonalOperator(full(self.position.domain, 0.))
        for energy in self.energy_list:
            curvature += energy.curvature
        curvature = self.ScaleOp(curvature)
        return InversionEnabler(curvature, self.parameters['inverter'])
