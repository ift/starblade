# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2017-2018 Max-Planck-Society
# Author: Jakob Knollmueller
#
# Starblade is being developed at the Max-Planck-Institut fuer Astrophysik


import nifty4 as ift

from .starblade_energy import StarbladeEnergy
from .starblade_kl import StarbladeKL


def build_starblade(data, alpha=1.5, q=1e-10, cg_steps=100, newton_steps=100,
                    manual_power_spectrum=None):
    """ Setting up the StarbladeEnergy for the given data and parameters
    Parameters
    ----------
    data : array
        The data in a numpy array
    alpha : float
        The slope parameter of the point source prior (default: 1.5).
    q : float
        The cutoff parameter of the point source prior (default: 1e-10).
    cg_steps : int
        Maximum number of conjugate gradient iterations for numerical operator inversion (default: 10).
    newton_steps : int
        Number of consecutive Newton steps within one algorithmic step.(default: 100)
    manual_power_spectrum : None, Field or callable
        Option to set a manual power spectrum which is kept constant during the separation.
        If it is not specified, the algorithm will try to infer it via critical filtering.
    sampling_steps : int
        Number of conjugate gradient steps for each sample (default: 1000).

    """

    s_space = ift.RGSpace(data.shape)
    h_space = s_space.get_default_codomain()
    data = ift.Field(s_space, val=data)
    FFT = ift.FFTOperator(h_space, target=s_space)
    binbounds = ift.PowerSpace.useful_binbounds(h_space, logarithmic=False)
    p_space = ift.PowerSpace(h_space, binbounds=binbounds)
    if manual_power_spectrum is None:
        initial_spectrum = ift.Field(p_space, val=1e-8)
        update_power = True
    else:
        initial_spectrum = manual_power_spectrum
        update_power = False
    initial_x = ift.Field(s_space, val=-1.)
    alpha = ift.Field(s_space, val=alpha)
    q = ift.Field(s_space, val=q)
    ICI = ift.GradientNormController(iteration_limit=cg_steps,
                                     tol_abs_gradnorm=1e-5)
    inverter = ift.ConjugateGradient(controller=ICI)

    parameters = dict(data=data, power_spectrum=initial_spectrum,
                      alpha=alpha, q=q,
                      inverter=inverter, FFT=FFT,
                      newton_iterations=newton_steps, update_power=update_power)
    Starblade = StarbladeEnergy(position=initial_x, parameters=parameters)
    return Starblade


def starblade_iteration(starblade, samples=5, cg_steps=100, newton_steps=100):
    """ Performing one Newton minimization step
    Parameters
    ----------
    starblade : StarbladeEnergy
        An instance of an Starblade Energy
    samples : int
        Number of samples drawn in order to estimate the KL. If zero the MAP is calculated (default: 5).
    cg_steps : int
        Maximum number of conjugate gradient iterations for
        numerical operator inversion (default: 100).
    newton_steps : int
        Number of consecutive Newton steps within one algorithmic step.(default: 100)
    """
    controller = ift.GradientNormController(name="Newton",
                                            tol_abs_gradnorm=1e-8,
                                            iteration_limit=newton_steps)
    minimizer = ift.RelaxedNewton(controller=controller)
    ICI = ift.GradientNormController(iteration_limit=cg_steps, tol_abs_gradnorm=1e-5)
    inverter = ift.ConjugateGradient(controller=ICI)
    para = starblade.parameters
    para['inverter'] = inverter
    energy = StarbladeEnergy(starblade.position, parameters=para)

    sample_list = []
    for i in range(samples):
        sample = energy.curvature.draw_sample(from_inverse=True)
        sample_list.append(sample)
    if len(sample_list) > 0:
        energy = StarbladeKL(starblade.position, samples=sample_list, parameters=energy.parameters)
    else:
        energy = starblade
    energy, convergence = minimizer(energy)
    energy = StarbladeEnergy(energy.position, parameters=energy.parameters)
    sample_list = []
    for i in range(samples):
        sample = energy.curvature.draw_sample(from_inverse=True)
        sample_list.append(sample)
    if len(sample_list) == 0:
        sample_list.append(energy.position)

    new_position = energy.position
    new_parameters = energy.parameters
    if energy.parameters['update_power']:
        new_power = update_power(energy)
        new_parameters['power_spectrum'] = new_power

    NewStarblade = StarbladeEnergy(new_position, new_parameters)
    return NewStarblade


def update_power(energy):
    """ Calculates a new estimate of the power spectrum given a StarbladeEnergy or StarbladeKL.
    For Energy the MAP estimate of the power spectrum is calculated and for KL the variational estimate.
    ----------
    energy : StarbladeEnergy or StarbladeKL
        An instance of an StarbladeEnergy or StarbladeKL

    """
    if isinstance(energy, StarbladeKL):
        power = 0.
        for en in energy.energy_list:
            power += ift.power_analyze(energy.parameters['FFT'].inverse(en.s),
                                       binbounds=en.parameters['power_spectrum'].domain[0].binbounds)
        power /= len(energy.energy_list)
    else:
        power = ift.power_analyze(energy.FFT.inverse(energy.s),
                                  binbounds=energy.parameters['power_spectrum'].domain[0].binbounds)
    return power


if __name__ == '__main__':
    pass
