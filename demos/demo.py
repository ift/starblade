# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2017-2018 Max-Planck-Society
# Author: Jakob Knollmueller
#
# Starblade is being developed at the Max-Planck-Institut fuer Astrophysik

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
import nifty4 as ift
from scipy.stats import invgamma
import starblade as sb


def generate_mock_data():
    s_space = ift.RGSpace([128,128])
    h_space = s_space.get_default_codomain()
    FFT = ift.FFTOperator(h_space)
    p_spec = lambda k: (1./(1+k)**4)
    S = ift.create_power_operator(h_space, power_spectrum=p_spec)
    sh = S.draw_sample()
    s = FFT(sh)
    q = 1e-3
    alpha = 1.5
    brightness = ift.log(ift.Field(s_space, val=invgamma.rvs(alpha-1., scale=q, size=s_space.shape)))

    u = brightness
    d = ift.exp(s) + ift.exp(u)
    return d.val

if __name__ == '__main__':
    np.random.seed(42)
    data = generate_mock_data()
    myStarblade = sb.build_starblade(data=data, alpha=1.5, q=1e-3)

    for i in range(5):  # not fully converged after 5 steps.
        myStarblade = sb.starblade_iteration(myStarblade, samples=5, cg_steps=100,
                                             newton_steps=50)

    ### PLOTS ###
    vmin = data.min()
    vmax = data.max()*0.1
    size = 15
    # plot data
    plt.figure()
    plt.imshow(data, norm=LogNorm(), vmin=vmin, vmax=vmax)
    cbar = plt.colorbar()
    cbar.set_label('intensity', size=size)
    plt.axis('off')
    plt.title('data', size=size)
    plt.savefig('data.pdf')
    plt.close()
    # plot diffuse component
    plt.figure()
    plt.imshow(np.exp(myStarblade.s.val), norm=LogNorm(), vmin=vmin, vmax=vmax)
    cbar = plt.colorbar()
    cbar.set_label('intensity', size=size)
    plt.axis('off')
    plt.title('diffuse component', size=size)
    plt.savefig('diffuse.pdf')
    plt.close()
    # plot point-like component
    plt.figure()
    plt.imshow(np.exp(myStarblade.u.val), norm=LogNorm(), vmin=vmin, vmax=vmax)
    cbar = plt.colorbar()
    cbar.set_label('intensity', size=size)
    plt.axis('off')
    plt.title('point-like component', size=size)
    plt.savefig('points.pdf')
    plt.close()
