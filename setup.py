from setuptools import setup

exec(open('starblade/version.py').read())

setup(name="starblade",
      version=__version__,
      author="Jakob Knollmueller",
      author_email="jakob@mpa-garching.mpg.de",
      description="",
      url="https://gitlab.mpcdf.mpg.de/ift/PointSourceSeparator",
      packages=["starblade"],
      zip_safe=True,
      dependency_links = [],
      install_requires = ["nifty4>=4.0","scipy>=0.17"],
      license="GPLv3",
      classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3 "
        "or later (GPLv3+)"
    ],)
